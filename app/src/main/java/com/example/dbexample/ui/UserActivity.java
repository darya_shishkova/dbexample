package com.example.dbexample.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.dbexample.App;
import com.example.dbexample.R;
import com.example.dbexample.db.DatabaseHelper;
import com.example.dbexample.db.model.Person;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static java.lang.Character.isLetter;

public class UserActivity extends AppCompatActivity {

    @BindView(R.id.firstName)
    EditText firstNameEdit;
    @BindView(R.id.surname)
    EditText surnameEdit;
    @BindView(R.id.secondName)
    EditText secondNameEdit;

    @BindView(R.id.day)
    Spinner daySpinner;
    @BindView(R.id.month)
    Spinner monthSpinner;
    @BindView(R.id.year)
    Spinner yearSpinner;

    @BindView(R.id.delete_button)
    Button delButton;

    DatabaseHelper databaseHelper;

    long userId = 0;

    Person person;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        ButterKnife.bind(this);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        databaseHelper = App.getInstance().getDatabaseInstance();

        firstNameEdit.setFilters(myFilter);
        surnameEdit.setFilters(myFilter);
        secondNameEdit.setFilters(myFilter);

        Bundle extras = getIntent().getExtras();
        if (extras != null){
            userId = extras.getLong("id");
        }
        if (userId > 0) {
            person = databaseHelper.getDataDao().getById(userId);
            firstNameEdit.setText(person.getFirstName());
            surnameEdit.setText(person.getSurname());
            secondNameEdit.setText(person.getSecondName());
            daySpinner.setSelection(person.getDay());
            monthSpinner.setSelection(person.getMonth());
            yearSpinner.setSelection(person.getYear());
        }else{
            person = new Person();
            // скрываем кнопку удаления
            delButton.setVisibility(View.GONE);
        }

    }

    @OnClick(R.id.save_button)
    public void onSaveClick(){
        String firstName = firstNameEdit.getText().toString();
        String surname = surnameEdit.getText().toString();
        String secondName = secondNameEdit.getText().toString();
        if(!firstName.equals("")&&!surname.equals("")) {
            person.setFirstName(firstName);
            person.setSurname(surname);
            person.setSecondName(secondName);
            person.setDay((int) daySpinner.getSelectedItemId());
            person.setMonth((int) monthSpinner.getSelectedItemId());
            person.setYear((int) yearSpinner.getSelectedItemId());
            if (userId > 0) {
                databaseHelper.getDataDao().update(person);
            } else {
                databaseHelper.getDataDao().insert(person);
            }
            finish();
        }
    }

    @OnClick(R.id.delete_button)
    public void delete(View view){
        databaseHelper.getDataDao().delete(person);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        this.finish();
        return super.onOptionsItemSelected(item);
    }
    public static InputFilter[] myFilter = new InputFilter[] {
            (source, start, end, dest, dstart, dend) -> {
                for (int i = start; i < end; i++) {
                    if (!isLetter(source.charAt(i))) {
                        return "";
                    }
                }
                return null;
            }
    };
}
