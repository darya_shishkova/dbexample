package com.example.dbexample.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.TextView;

import com.example.dbexample.App;
import com.example.dbexample.R;
import com.example.dbexample.db.DatabaseHelper;
import com.example.dbexample.ui.adapter.SomeDataRecyclerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements SomeDataRecyclerAdapter.AdapterCallback {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    String[] dayArr;
    String[] monthArr;
    String[] yearArr;

    long id = 0;

    private DatabaseHelper databaseHelper;

    TextView header;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        dayArr = getResources().getStringArray(R.array.day);
        monthArr = getResources().getStringArray(R.array.month);
        yearArr = getResources().getStringArray(R.array.year);

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false));
        databaseHelper = App.getInstance().getDatabaseInstance();

        SomeDataRecyclerAdapter recyclerAdapter = new SomeDataRecyclerAdapter(this, databaseHelper.getDataDao().getAll(), dayArr, monthArr, yearArr);
        recyclerView.setAdapter(recyclerAdapter);
        recyclerAdapter.setOnItemClickListener((itemView, position, view) -> {
            Intent intent = new Intent(getApplicationContext(), UserActivity.class);

            intent.putExtra("id", id);
            startActivity(intent);
        });

        header = findViewById(R.id.header);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            Intent intent = new Intent(this, UserActivity.class);
            startActivity(intent);
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        SomeDataRecyclerAdapter recyclerAdapter = new SomeDataRecyclerAdapter(this, databaseHelper.getDataDao().getAll(), dayArr, monthArr, yearArr);
        recyclerView.setAdapter(recyclerAdapter);
        String msg = getString(R.string.message) + " " + recyclerAdapter.getItemCount();
        header.setText(msg);
    }

    @Override
    public void onMethodCallback(long id) {
        this.id = id;
    }


    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem menuItem = menu.findItem(R.id.searh_view);
        SearchView searchView =(SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                String likeText  ="%"+s+"%";
                SomeDataRecyclerAdapter recyclerAdapter = new SomeDataRecyclerAdapter(MainActivity.this, databaseHelper.getDataDao().getPersonByKeyword(likeText), dayArr, monthArr, yearArr);
                recyclerView.setAdapter(recyclerAdapter);
                return false;
            }
        });
        return true;
    }
/*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/
}
