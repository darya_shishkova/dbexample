package com.example.dbexample.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dbexample.R;
import com.example.dbexample.db.model.Person;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.support.v7.widget.RecyclerView.ViewHolder;

public class SomeDataRecyclerAdapter extends RecyclerView.Adapter<ViewHolder> {

    private List<Person> personList;
    private String[] dayArray;
    private String[] monthArray;
    private String[] yearArray;

    private AdapterCallback mAdapterCallback;

    public SomeDataRecyclerAdapter(Context context, List<Person> personList, String[] dayArray, String[] monthArray, String[] yearArray) {
        this.personList = personList;
        this.dayArray = dayArray;
        this.monthArray = monthArray;
        this.yearArray = yearArray;
        try {
            this.mAdapterCallback = ((AdapterCallback) context);
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement AdapterCallback.");
        }
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_some_data, parent, false);
        return new NewsViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final NewsViewHolder viewHolder = (NewsViewHolder) holder;
        viewHolder.title.setText(String.format("%s %s %s", personList.get(position).getFirstName(), personList.get(position).getSurname(), personList.get(position).getSecondName()));
        String description = dayArray[personList.get(position).getDay()] + " " + monthArray[personList.get(position).getMonth()];
        if(personList.get(position).getYear() > 0) {
            viewHolder.description.setText(String.format("%s %s", description, yearArray[personList.get(position).getYear()]));
        }else{
            viewHolder.description.setText(description);
        }
    }



    @Override
    public int getItemCount() {
        return personList.size();
    }


    private static OnItemClickListener listener;

    public interface OnItemClickListener{
        void onItemClick(View itemView, int position, View view);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        SomeDataRecyclerAdapter.listener = listener;
    }

    public class NewsViewHolder extends ViewHolder implements View.OnClickListener{

        @BindView(R.id.title)
        public TextView title;
        @BindView(R.id.description)
        public TextView description;

        NewsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> {
                if (SomeDataRecyclerAdapter.listener!=null){
                    mAdapterCallback.onMethodCallback(personList.get(getLayoutPosition()).getId());
                    SomeDataRecyclerAdapter.listener.onItemClick(itemView, getLayoutPosition(), v);
                }
            });
        }

        @Override
        public void onClick(View view) {
            SomeDataRecyclerAdapter.listener.onItemClick(itemView, getLayoutPosition(), view);
        }
    }
    public interface AdapterCallback {
        void onMethodCallback(long id);
    }
}
