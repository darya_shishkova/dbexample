package com.example.dbexample.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.dbexample.db.model.Person;

import java.util.List;

@Dao
public interface PersonDao {
    @Query("SELECT * FROM person")
    List<Person> getAll();

    @Query("SELECT * FROM person WHERE id = :id")
    Person getById(long id);

    @Query ("SELECT * FROM person WHERE firstName LIKE :firstName OR surname LIKE :firstName " +
            "OR secondName LIKE :firstName OR day LIKE :firstName OR month LIKE :firstName " +
            "OR year LIKE :firstName")
    List<Person> getPersonByKeyword(String firstName);

    @Insert
    void insert(Person person);

    @Update
    void update(Person person);

    @Delete
    void delete(Person person);
}
